# Links of all shared Google Documents

1. Teams Catalog
     https://docs.google.com/document/d/1wibA-HXdF4S1oS2pQ5M6tXzBiIHclP-1qruL01ZiGGg/edit?

2. Creative Meeting
     https://docs.google.com/document/d/1uDaEvfVtxXh_sSYFSa3TtLjhOaUVZCFVY_1sRtol3k4/edit?

3. Customer Delivery
     https://docs.google.com/document/d/1yExkVUvi_E5g3tqm1r70bT09zJY1YNQPoLMAmxqP3R8/edit?

4. aiRender Website Dev
     https://docs.google.com/document/d/1D54o0VZcmuh-454-1_Np7gcj3NjS8xBHsIXXYCOduZE/edit?
 
5. Post on LinkedIn
     https://docs.google.com/document/d/1H5A5F5kBvSP--R9xXHlncxupyZ9ZwN1hL-UHZTRLV94/edit?

6. Instagram Work
     https://docs.google.com/document/d/1AuvzuJWKz0o5u5eD6TJBmcS2N6vNK23m0urpaCG-aU0/edit

7. Unreal Rendering
     https://docs.google.com/document/d/1Dlxc-vfmhWWSo1MJtUUz02l0Zv-J_XaCI6Vp-Jsh3TA/edit#heading=h.ufxfxqnmh4px

8. Google Meet Vs hi > 
     https://docs.google.com/document/d/1AVMaXhHEiOMuwH0DBYdhOEs-sH26_AR0jCDqaZxZadg/edit

9. VR Headset doc > 
     https://docs.google.com/document/d/17tn9CsRxpeucne3S4p_-CAS8BuIMPy8bGdT72d5cfjg/edit

10. ML PPT Notes > 
     https://docs.google.com/document/d/1qBDh7kqU-CGhfVcQqL6g4pEUhb1BbrIaOxh04irpkEE/edit

11. Making Video/Trailer > 
     https://docs.google.com/document/d/11wgvdB2VJXauFRpcPGnvLwyMuUDhd3-K_L2qsKO0GWg/edit

12. AWS - IBM Cloud instances  > 
     https://docs.google.com/document/d/1gvPx9fPV58larvQSl7NnygsHbMN7QQL-MY6qrMg-kms/edit

13. video-app (Google Meet Vs hi) > 
     https://docs.google.com/document/d/1c46EKJwOAtNFRrgh0eAfwNi7U2jONuu8zs7EqAD4Jow/edit

14. RTWO hi > 
     https://docs.google.com/document/d/1MR_M8f6LaLjZXBTZzA2niX5iOCzzVWCNOrQiT1inq_g/edit

15. Casual conversations > 
     https://docs.google.com/document/d/1fqFBNlr9hcITsh1dYXiiu_KNAOMCpFjf8YdiUgbsNH0/edit

16. Sales and Marketting > 
     https://docs.google.com/document/d/1gqjQSRoVSgC6ys8n38HbeUnPW7-k86j0zFxl6kLL8mw/edit

17. Metrics Tracking  > 
     https://docs.google.com/document/d/1z2JOG_SiZ4wbpKo_jz_pBT9g_4oUR8ofShQ3Ug5iLiQ/edit

18. Server Design > 
     https://docs.google.com/document/d/1LoYcP50OlhkGG9CIGHgiG6jLHb_dRyfsxIdB3HRpaCc/edit#heading=h.4vn3ashzlqz8

19. Lohith / Nelesh Tasks Notes > 
     https://docs.google.com/document/d/1QlW6qV2blxQxEuzee5b7jEElS8Ez3vvpa6n-o_ZHFPU/edit#heading=h.sfle58cxlk4o

20. USP hi sales
    https://docs.google.com/document/d/1ZmvB_C1P8It2vueotJM98d8T54bkyGk4vATsS8dvtr8/edit

21. Terms to Recall: GoogleNet
    https://docs.google.com/document/d/1FJXO4bh_6jjF7EkbhkAkk3l2aBvFzDM45_Iuy1gft5w/edit?usp=sharing

22. hi Frontend & Backend Documentation
    https://docs.google.com/document/d/1_qFherP0P7C38xq9T6rUMjQ_HY_dcWpN6YxBMbhicYI/edit?tab=t.ha4zexwro8e0
    https://drive.google.com/drive/folders/1lbgSiJwknnwQQT24O_KFovYDVAunGwuw (drive)

# Links of all shared Google Sheets

   APP TESTING >>
   https://docs.google.com/spreadsheets/d/10o2xjSOs-ge9ZV1JM2ZmsuaxbCnDL1k1M2AZHN1h2eU/edit#gid=367704591

1. IBM Cloud Instance    >
   https://docs.google.com/spreadsheets/d/1cC-UeDitcP45P4tJxUeEXZ9YZn9NlUauEX5suFXfSXQ/edit

2. Influencers >
   https://docs.google.com/spreadsheets/d/10SRT8i4RK4F9bdylPCGCbaR2mMYebSeeBvdbGUa018g/edit

3. Sales contact    >
   https://docs.google.com/spreadsheets/d/1ydE2hppaQkNiyGwQw_YExvCsqH-YbI59IZcKH7dFRBw/edit

4. Priority List    >
   https://docs.google.com/spreadsheets/d/1eZ9x98QXA5qAICC7AF-XLJAYgqLxx-uNvcQuTihQTQs/edit

5. 2d-to-3d conversion working steps    >
   https://docs.google.com/spreadsheets/d/1uIXJtLlrtVLsf2YKfh1E1DaQ8VSQluJkITnC-NKcLaw/edit

6. CUSTOMER DISCOVERY -LinkdedIn   >
   https://docs.google.com/spreadsheets/d/1KpPMCBPE47OJ1Av_m7y_V99tZTi2LTPkHoTkDsvHEHM/edit

7. Bugs   >
   https://docs.google.com/spreadsheets/d/1bx1fd1PkmMPz4tBYYyL8gmLMVEjkF3pmi0uXf0qvaHY/edit

8. CasualConversationsPricing      >
   https://docs.google.com/spreadsheets/d/1TGiwFiFqf8MKpOoqLxeoabTaO5QezLzQFcnf9YWXv-g/edit

9. aiRender Constent form 
   https://docs.google.com/spreadsheets/d/10O4oDidiaJ3I8ArB8zpSzo2znxaLh8e92MLwo2Vlunc/edit#gid=714647871

10. Azure 
    https://docs.google.com/spreadsheets/d/1sHDL6XB-Q0w3eVz7tbpV8z9YwYEzYOpYy3Wr0wn6enU/edit?usp=sharing

11. IBM Cloud Task
    https://docs.google.com/spreadsheets/d/1cC-UeDitcP45P4tJxUeEXZ9YZn9NlUauEX5suFXfSXQ/edit?usp=sharing

12. Youtubers List for 3d Conversion
    https://docs.google.com/spreadsheets/d/1tMJnOOPkbv9CBjoKA9LADRAOuYd4rbM4TfpystEaSno/edit?usp=sharing

13. Detection of holes (figures)
    https://docs.google.com/document/d/1Lx8gnsNZkldCfFRTu8pQAqy6gYkCr6rZ8JXH0s93LYU/edit?usp=sharing

14. 

# hiRender
1. hiRender-Team2 (Deekshitha's Team)
   https://docs.google.com/document/d/1y-7OI2alm73dhNijgvoFP0eglPJuFHN_NfMUNe21JX4/edit?usp=sharing

2. hiRender-Team2 -Individual Day Task Deadline List
   https://docs.google.com/spreadsheets/d/1qEa2hqHOSVNRYbcJKm8BQwaKvOgCBurhrDy1WZW5EBc/edit?usp=sharing
   https://www.notion.so/Search-Model-papers-a46a3b45f9d744c0b612ccdaaeb95ed2 (not used much)

3. hiRender - web dev team( Lohith's Team)
   https://docs.google.com/spreadsheets/d/1wOE-kIH4SGM39OFBpJucVqLzt2x4zHppmEZUBKuxb1A/edit?usp=sharing


 
# Meeting Minutes Sheet for all Department

1. Team Catalog
   https://docs.google.com/spreadsheets/d/1Pwo-9j0WjcaIh09ZFCFEj_9tQ7vB1pWSF90fZ3mxPsM/edit?usp=sharing



# Sales Pitch Deck
1. https://www.canva.com/design/DAFk9HWwY-0/iWUlqLyOqws3aftLhPZuUw/edit

2. https://docs.google.com/document/d/18bhd5M60YjkgLz_N8CjdqgVIAB9wgRAQijrUC2ia7Rg/edit?usp=sharing

3. https://www.shutterstock.com/image-vector/wheelchair-isolated-man-woman-flat-3d-365683628


# Future Work

1. hiRender-future
   https://docs.google.com/document/d/1G9rRSa9Hk1vZfrXobiTfk_T6AZmyuKnZ_86vBdl4l1Y/edit?usp=sharing

2. hiRender-Demo
   https://docs.google.com/document/d/1oBc0wCmFZ5HGQV3hE4uXwW6R-Z3dALPxJ-JMHGgdHNc/edit?usp=sharing
   

  



