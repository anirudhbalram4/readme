
# Problem   : 
### I couldn't clone the repository from existing GitLab project(say 3D Fundamentals & I have Developer role permissions) and I get this error when I push to existing repo:
```
      " Permission denied (publickey) fatal : Could not read from remote repository "
```

#
# Solved this issue!  

## Using Git Bash
Check whether you have  SSH key that connects your PC to your personal GitLab account
If you **don't** have then you cannot push/clone from/to Gitlab/Local System.
# How to check whether the SSH key(public-privat key) is set/not?
1. Type :
   ```
   ssh -vvv git@gitlab.com 
   ```
and you will get one of the statement as welcome message like this :
```
Welcome to GitLab, <your user-name!>
```
If you get this message, then your SSH key is set to your GitLab account with "your user-name". 

If not follow the steps below:

# How to Create a SSH key?
1. Open Git bash as "Run as Administrator" and Follow the steps from here: https://stackoverflow.com/questions/40427498/getting-permission-denied-public-key-on-gitlab

2. Edit the config file in the .ssh folder and add:
   ```
   Host *
    ServerAliveInterval 60
   ```
(Note: Space & new-line character matters in the above code. Copy the same!). 

* If you don't find the config file in .ssh folder(c:/Users/yourPC-user-name/.ssh), then create one without any extensions:
   ```
   vim config

   cat config
   ```

You can also check whether the SSH key is set/not.

(For more information, visit: https://www.fosstechnix.com/how-to-add-ssh-key-to-gitlab/)

# How to clone the repository, Create a branch, and later push into GitLab repository with this new branch?

List of commands to be used :
```
git config --global user.name "<your gitlab username>"
```
```
git config --global user.email <your gitlab emailId>
```
```
git config --list
```
1.  Create a folder in one of your directories where you want to clone and maintain the gitlab repo locally.
      ```
      cd /c/<your folder-name>/
      ```
2. After this enter within <your folder-name> folder and type :
   ```
   cd <your folder-name>

   git clone git@gitlab.com:chaithanyasree/3d-fundamentals.git
   ```
Instead of this repo which is given as an example, you can clone your own repo.

3. After clone is successfull, 
Get into the folder <your cloned-repo-name>

   ```
   cd 3d-fundamentals/
   ls
   ```

## How to create a new Branch ?
   ```
   git checkout -b <your new Branch-name>
   ```

## How to see if the branch is already created in the repo ?
   ```
   git branch -av
   ```

## How to Check-out an existing branch and pull?
   ```
   git checkout <Your new Branch-name or an existing Branch-name>

   git pull
   ```

## How to Check-in code into the existing repo ?
   ```
   git status

   git add .
      
   git status
      
   git commit -m "Assignments of Tensorflow:Advanced Techniques C1->Custom models,layers and Functional API's C2->GAN "
      
   git status
      
   git push --set-upstream origin <your branch-name>
      
   git status
   ```
## ***Congratulations! Now, You should be able to clone the repo from GitLab locally and also can push/pull repo to/from GitLab project.***

# References

1.  https://stackoverflow.com/questions/40427498/getting-permission-denied-public-key-on-gitlab
2. https://www.fosstechnix.com/how-to-add-ssh-key-to-gitlab/
3. https://forum.gitlab.com/t/permission-denied-publickey/29670
4. https://stackoverflow.com/questions/45174228/git-error-key-does-not-contain-a-section
```
** Some of these commands maybe helpful: **

ls -alh ~/.ssh

mkdir <new folder-name>

cd <new folder-name>

ls -alhtr

cat <your file-name>

** This below command gets you list of all commands applied earlier: **

history

history | grep clone
```
