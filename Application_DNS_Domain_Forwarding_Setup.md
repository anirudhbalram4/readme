# To setup an application in Cloud server


## 1. Buying Domain

+ Goto goDaddy buy for a new domain.


## 2. Setup  Cloud Server

+  Select a cloud service provider like Gcloud or AWS or Azure. 
+  Create a new server with the required specs.
+  Install the required software.
+  Configure the server settings like Inbound and outbound rules in their respective GUI.
+  Get the public IP address of this server.


## 3. Setup Cloud DNS 
+ Either setup cloud DNS in goDaddy or cloud service providers (like Gcloud/AWS/Azure) where you have bought the server. Remember, you can do  domain forwarding from the service where DNSZONE is established.
+ For example :
    + Lets say: we have a server with **"appA"** in **Gcloud**. We setup DNS zone, records in **GoDaddy**.
    +  Goto **Domains** ->sScroll to your domain in the list which you want to setup the DNS."
    +  Select **DNS**  or  **Manage** button beside it.
    +  Under **"DNS"** -> select **"Nameservers"** -> Select **"Change nameservers"** -> Select **"Go Daddy Nameservers(recommended)"**
    + Automatically, Default DNS records are set under **DNS record** tab
    + Under **DNS Records**, Change **A** record with **value = <ip_address>**. **<ip_address>** is the  public IP address of your server in Gcloud.

## 4. Setup Forwarding Domain [If necessary, else skip this step]
+ We had setup the forwarding domain or domain mapping using GoDaddy services.
    + Let say we want to do domain mapping from **"appA.com"** to **"appA.gcp.com"**.
    + Go to **Domains** -> Scroll to your domain in the list which you want to setup
    + Select **DNS** or **Manage** button beside it.
    + Under **"DNS"** -> select **"Domain Forwarding"** -> Select *Add*"
    + In the dialog box, select **http://** (always) and Enter destination url without any "www" or any addition  like **http://appA.gcp.com**. Just type  the domain name **appA.gcp.com**
    + Next,  select **"Forward with masking"** if you want to do domain forwarding but hide the destination URL in browser address bar. Otherwise, select **permanent** or **temporary** to forward domain without hidding destination url in web browser address bar.
    + Under **Forward with masking** option, add title, description and keyword. Keyword is space  separated list of keywords which will be used for SEO purpose(Think on what customers would type to find your web page)
    + Click **Save** button to save the changes.
    + Now, you can see the domain mapping is done. But would take 24h-48h to reflect the changes globally.
    

References:
```
https://www.godaddy.com/en-in/help/whats-the-difference-between-domain-forwarding-and-masking-32147
https://www.godaddy.com/en-in/help/forward-my-godaddy-domain-12123
```