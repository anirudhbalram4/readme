# To check the expiry date of the ssl certificate
  openssl x509 -enddate -noout -in <filename.pem>

# To renew certbot certificates on remote Ubuntu PC (using certbot via Let's Encrypt)

## To install certbot
  apt install certbot


## To create SSL certificate for domain(s) [ Recommended ]
  If you want to create SSL certificate for domains like example.com ,www.example.com, proceed further.
  Intially, if customer types 'example.com' they would have seen a blank webpage with "refuse to connect" message. 
  
  To rectify this problem, run below command:
  
```
#Syntax
certbot certonly --standalone -d <domain_name_1> -d <domain_name_2>

#For example:
certbot certonly --standalone -d airender.co.in -d www.airender.co.in

#OR

certbot certonly --standalone -d airender.co.in,www.airender.co.in
```

# Path for the certificates
example: for www.airender.co is
/etc/letsencrypt/live/www.airender.co/../../archive/www.airender.co/
The dowloaded .zip contains certificate.crt, ca_bundle.crt, private.key.
Download the files and convert to fullchain.pem, privkey.pem, chain.pem, cert.pem
Example steps are given in https://stackoverflow.com/questions/991758/how-to-get-pem-file-from-key-and-crt-files
(not sure about the validity of the steps in above stackoverflow link. You'll have to verify.)


# To create ca_bundle file

### Create ca_bundle.pem file and copy the contents of cert.pem
```
cat cert.pem > ca_bundle.pem
```
### Append the contents of chain.pem to ca_bundle.pem
```
cat chain.pem >> ca_bundle.pem
```

# Verification

### To verify the webpage domain
For example: Take airender.co and www.airender.co
```
Goto "whynopadlock.com" website.

#In first tab:
Type "airender.co"
#See the SSL certifcate has "pass" indication
#If not, SSL certificate creation and domain mapping has failed!

#In second tab:
Type "www.airender.co"
#See the SSL certifcate has "pass" indication
#If not, SSL certificate creation and domain mapping has failed!


```


Renewing the SSL certificate is successful!













# To create SSL certificate for only one domain 
## To renew certbot certificates on ONLY 1  on remote Ubuntu PC
  sudo certbot certonly --standalone

## Issues/Errors,if found:
1.  While renewing the certificate:
```
root@virtualserver01:~# sudo certbot certonly --standalone
Saving debug log to /var/log/letsencrypt/letsencrypt.log
Plugins selected: Authenticator standalone, Installer None
/usr/local/lib/python3.8/dist-packages/cryptography/hazmat/backends/openssl/x509.py:14: CryptographyDeprecationWarning: This version of cryptography contains a temporary pyOpenSSL fallback path. Upgrade pyOpenSSL now.
  warnings.warn(
    Please enter in your domain name(s) (comma and/or space separated)  (Enter 'c'
to cancel): c

```
Solution:
```
#cerbot was installed via "apt install cerboot" version of the certbot at the time of writing is 
#certbot=0.40.0 (required)
#cryptography=36.0.2 (required)

pip install pip --upgrade
pip install pyopenssl --upgrade

#Now, pyOpenSSL=19.0.0--> is upgarde to 23.0.0 (required)
```
2. "AttributeError: module 'lib' has no attribute 'X509_V_FLAG_CB_ISSUER_CHECK'"

Solution:
```
a. Edit the crypto.py file mentioned in the stacktrace and remove the offending line by commenting it out with a #

Then upgrade latest version of PyOpenSSL.

b. pip install pip --upgrade
c. pip install pyopenssl --upgrade

Now you can re-add the commented line again and it should be working
```

3. " AttributeError: module 'lib' has no attribute 'X509_get_notAfter' "

```
root@airender-website:~/WORK/KEYS#  sudo certbot certonly --standalone
Saving debug log to /var/log/letsencrypt/letsencrypt.log
Plugins selected: Authenticator standalone, Installer None
/usr/local/lib/python3.8/dist-packages/cryptography/hazmat/backends/openssl/x509.py:14: CryptographyDeprecationWarning: This version of cryptography contains a temporary pyOpenSSL fallback path. Upgrade pyOpenSSL now.
  warnings.warn(
Please enter in your domain name(s) (comma and/or space separated)  (Enter 'c'
to cancel): www.airender.co.in
An unexpected error occurred:
AttributeError: module 'lib' has no attribute 'X509_get_notAfter'
Please see the logfiles in /var/log/letsencrypt for more details.
```


Solution:
```
root@airender-website:~/WORK/KEYS# pip install pyopenssl --upgrade
```



4. If you get an error like " Problem binding to port 80: Could not bind to IPv4 or IPv6." then run:
```
sudo lsof -i -P -n | grep LISTEN
```
If port 80 is currently in LISTEN mode find the server which is using this port and stop it.
In our case runHttpVerifyServer.sh was running using pm2.So, we stopped it using 
```
sudo pm2 stop runHttpVerifyServer.sh
```
If apache2/nginx is running stop using 
```
sudo systemctl stop apache2
or 
sudo systemctl stop nginx
```
then run :
```
sudo certbot certonly --standalone
```
After that you can start apache2 or nginx if you want using 
```
sudo systemctl start apache2
or 
sudo systemctl start nginx
```
A successful certbot looks like this:
```
ubuntu@ip-172-31-46-30:~/rwto_server/CERT/21_7_2022/sslcert$ sudo certbot certonly --standalone
Saving debug log to /var/log/letsencrypt/letsencrypt.log
Plugins selected: Authenticator standalone, Installer None
Please enter in your domain name(s) (comma and/or space separated)  (Enter 'c'
to cancel): www.airender.co.in
Cert not yet due for renewal

You have an existing certificate that has exactly the same domains or certificate name you requested and isn't close to expiry.
(ref: /etc/letsencrypt/renewal/www.airender.co.in.conf)

What would you like to do?
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
1: Keep the existing certificate for now
2: Renew & replace the cert (limit ~5 per 7 days)
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Select the appropriate number [1-2] then [enter] (press 'c' to cancel): 2
Renewing an existing certificate
Performing the following challenges:
http-01 challenge for www.airender.co.in
Waiting for verification...
Cleaning up challenges

IMPORTANT NOTES:
 - Congratulations! Your certificate and chain have been saved at:
   /etc/letsencrypt/live/www.airender.co.in/fullchain.pem
   Your key file has been saved at:
   /etc/letsencrypt/live/www.airender.co.in/privkey.pem
   Your cert will expire on 2022-10-19. To obtain a new or tweaked
   version of this certificate in the future, simply run certbot
   again. To non-interactively renew *all* of your certificates, run
   "certbot renew"
 - If you like Certbot, please consider supporting our work by:

   Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
   Donating to EFF:                    https://eff.org/donate-le

```
Just verify if fullchain.pem and privkey.pem link files map to latest corresponding files in 
/etc/letsencrypt/live/www.airender.co.in/../../archive/www.airender.co.in

For example:
In Machine6 (ssh -i Machine6.pem ubuntu@ec2-35-154-140-58.ap-south-1.compute.amazonaws.com) for Webiste: www.airender.co.in
(as of 21/7/2022 website is hosted in this AWS machine)
Run these commands:
```
ls -alhtr ~/cert_keys_web
sudo ls -alhtr /etc/letsencrypt/live/www.airender.co.in
sudo ls -alhtr /etc/letsencrypt/live/www.airender.co.in/../../archive/www.airender.co.in
```

## ZeroSSL steps to create / renew SSL certificates
1. Goto  https://app.zerossl.com/ and click on new certificate and follow the steps
2. Go for 90-day free-certificate.
3. Domain name= www.airender.co.in or which ever domain name you want to use.
4. For Verification step : Select HTTP file upload option
     *  Download the Auth file (say 3D8FF8FE545DD11663BB49D923A4E0B4.txt)
     *  Open a Git Bash (1) and open path /home/ubuntu/rwto_server
     *  Copy the downloaded auth file from your device to server path=/home/ubuntu/rwto_server/Certificates 
     using scp.
     *  Open the httpVerifyServer.sh and edit the path in above two lines:
      ```
      if(req.method == 'GET' && req.url == '/.well-known/pki-validation/3D8FF8FE545DD11663BB49D923A4E0B4.txt')
      var text = fs.readFileSync('certificates/3D8FF8FE545DD11663BB49D923A4E0B4.txt');
      ```
      * Save and quit the above file.
      * Type in the git bash(1)
      ```
      sudo pm2 start runHttpVerifyServer.sh  
      ```
5. Go back to ZeroSSL webiste page and click on the link given in last step. See if this website displays.
6. Return to ZeroSSL website and click next step.
7. Now click on Verify Domain. If there's no error then certifcate is created.
8. Check your email with which you logged into ZeroSSL. There will be a certificate file to download.


# Path for the certificates
example: for www.airender.co is
/etc/letsencrypt/live/www.airender.co/../../archive/www.airender.co/
The dowloaded .zip contains certificate.crt, ca_bundle.crt, private.key.
Download the files and convert to fullchain.pem, privkey.pem, chain.pem, cert.pem
Example steps are given in https://stackoverflow.com/questions/991758/how-to-get-pem-file-from-key-and-crt-files
(not sure about the validity of the steps in above stackoverflow link. You'll have to verify.)



# To create ca_bundle file

### Create ca_bundle.pem file and copy the contents of cert.pem
```
cat cert.pem > ca_bundle.pem
```
### Append the contents of chain.pem to ca_bundle.pem
```
cat chain.pem >> ca_bundle.pem
```

Renewing the SSL certificate is successful!
