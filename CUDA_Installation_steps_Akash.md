# Installation of CUDA

1. Download CUDA toolkit - version 9.0.176

	https://developer.nvidia.com/cuda-downloads

2. Download cuDNN

3. Downloading the Visual Studio Community version - 2017 community version

	- https://quasar.ugent.be/files/doc/cuda-msvc-compatibility.html
	- installing Visual Studio 2017 Community --
		-> Desktop Development with C++

4. Anaconda command propmt
```
 conda create --name tfgpu36 python=3.6

 conda activate tfgpu36

 pip install tensorflow-gpu==1.6.0

 pip install -r requirements-pip.txt
```
