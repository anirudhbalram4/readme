## To install pm2
```
sudo npm i -g pm2
```
## To check the pm2 version
```
npx pm2 --version
```
## To run this script in background
```
sudo pm2 start run_new_website.sh
```
## To stop running the script
```
sudo pm2 stop run_new_website
```
## To list all your application
```
sudo pm2 list
```
## To check the status of the applications
```
sudo pm2 status
```

### For more information visit > https://www.tecmint.com/install-pm2-to-run-nodejs-apps-on-linux-server/

