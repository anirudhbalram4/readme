# hiThere -Issue found while "npm run build"
### Issue 1:

"> hi@0.1.0 build /mnt/500gb/ProductionServer/hithere
> react-scripts build

Creating an optimized production build...

<--- Last few GCs --->

[4117:0x85c9f0]    61468 ms: Scavenge 1334.8 (1433.5) -> 1327.3 (1436.5) MB, 21.0 / 0.0 ms  (average mu = 0.269, current mu = 0.191) allocation failure
[4117:0x85c9f0]    61547 ms: Scavenge 1337.2 (1436.5) -> 1331.4 (1444.0) MB, 15.0 / 0.0 ms  (average mu = 0.269, current mu = 0.191) allocation failure
[4117:0x85c9f0]    62903 ms: Mark-sweep 1344.1 (1444.0) -> 1327.4 (1440.5) MB, 1236.0 / 0.0 ms  (average mu = 0.234, current mu = 0.200) allocation failure scavenge might not succeed


<--- JS stacktrace --->

==== JS stack trace =========================================

    0: ExitFrame [pc: 0x3255e6c5452b]
Security context: 0x2a06af1aee11 <JSObject>
    1: getArg [0x25d070005e21] [/mnt/500gb/ProductionServer/hithere/node_modules/webpack-sources/node_modules/source-map/lib/util.js:~18] [pc=0x3255e7424bd0](this=0x25d070013239 <Object map = 0x1eccab304989>,aArgs=0x2237f3ba2991 <Object map = 0x2d2070789b01>,aName=0x2a06af1fe641 <String[8]: original>,aDefaultValue=0x1be7b7b025a9 <null>)
    2: /* anonymous *...

FATAL ERROR: Ineffective mark-compacts near heap limit Allocation failed - JavaScript heap out of memory
 1: 0x7fdefb1cf46c node::Abort() [/lib/x86_64-linux-gnu/libnode.so.64]
 2: 0x7fdefb1cf4b5  [/lib/x86_64-linux-gnu/libnode.so.64]
 3: 0x7fdefb3fbe6a v8::Utils::ReportOOMFailure(v8::internal::Isolate*, char const*, bool) [/lib/x86_64-linux-gnu/libnode.so.64]
 4: 0x7fdefb3fc0e1 v8::internal::V8::FatalProcessOutOfMemory(v8::internal::Isolate*, char const*, bool) [/lib/x86_64-linux-gnu/libnode.so.64]
 5: 0x7fdefb796c66  [/lib/x86_64-linux-gnu/libnode.so.64]
 6: 0x7fdefb7a8043 v8::internal::Heap::PerformGarbageCollection(v8::internal::GarbageCollector, v8::GCCallbackFlags) [/lib/x86_64-linux-gnu/libnode.so.64]
 7: 0x7fdefb7a8930 v8::internal::Heap::CollectGarbage(v8::internal::AllocationSpace, v8::internal::GarbageCollectionReason, v8::GCCallbackFlags) [/lib/x86_64-linux-gnu/libnode.so.64]
 8: 0x7fdefb7aa91d v8::internal::Heap::AllocateRawWithLigthRetry(int, v8::internal::AllocationSpace, v8::internal::AllocationAlignment) [/lib/x86_64-linux-gnu/libnode.so.64]
 9: 0x7fdefb7aa975 v8::internal::Heap::AllocateRawWithRetryOrFail(int, v8::internal::AllocationSpace, v8::internal::AllocationAlignment) [/lib/x86_64-linux-gnu/libnode.so.64]
10: 0x7fdefb776dda v8::internal::Factory::NewFillerObject(int, bool, v8::internal::AllocationSpace) [/lib/x86_64-linux-gnu/libnode.so.64]
11: 0x7fdefba0231e v8::internal::Runtime_AllocateInNewSpace(int, v8::internal::Object**, v8::internal::Isolate*) [/lib/x86_64-linux-gnu/libnode.so.64]
12: 0x3255e6c5452b"

npm ERR! code ELIFECYCLE
npm ERR! errno 1
npm ERR! hi@0.1.0 build: `react-scripts build`
npm ERR! Exit status 1
npm ERR!
npm ERR! Failed at the hi@0.1.0 build script.
npm ERR! This is probably not a problem with npm. There is likely additional logging output above.

### Solution 1 :

a) Check the size of the memory using top command (here 7GB)<br/>
```
top
```

b)  Run this command 
```
# X=1024,4096,(nultiples of 1024) MB
export NODE_OPTIONS="--max-old-space-size=<X>"

# I ran this command memory size of 7GB, this was enough for me
export NODE_OPTIONS="--max-old-space-size=4096"
npm run build

```
c) Go to hithere folder and do "npm run build"
```
npm start or pm2 start <run-prod-server.sh>

Congratulations! Successfully resolved the issue!
Happy Learning:-)
